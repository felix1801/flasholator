// Translate tab widget with a language selector, a text field for the word to translate, a text label to display the translation and a button to translate the word.
import 'package:flutter/material.dart';
import 'utils/flashcards_collection.dart';
import 'utils/deepl_translator.dart';

class TranslateTab extends StatefulWidget {
  final FlashcardsCollection flashcardsCollection;
  final DeeplTranslator deeplTranslator;
  final Function(Map<dynamic, dynamic>) addRow;
  final Function() updateQuestionText;

  const TranslateTab({
    Key? key,
    required this.flashcardsCollection,
    required this.deeplTranslator,
    required this.addRow,
    required this.updateQuestionText,
  }) : super(key: key);

  @override
  State<TranslateTab> createState() => _TranslateTabState();
}

class _TranslateTabState extends State<TranslateTab> {
  String _sourceLanguage = 'FR';
  String _targetLanguage = 'ES';
  String _wordToTranslate = '';
  String _translatedWord = '';
  bool isTranslateButtonDisabled = false;
  bool isAddButtonDisabled = true;

  void _swapContent() {
    setState(() {
      final String tmp = _sourceLanguage;
      _sourceLanguage = _targetLanguage;
      _targetLanguage = tmp;
    });
  }

  Future<void> _translate() async {
    isTranslateButtonDisabled = true;
    Map<String, dynamic> translation = await widget.deeplTranslator.translate(
      _wordToTranslate,
      _targetLanguage,
      _sourceLanguage,
    );
    isTranslateButtonDisabled = false;
    isAddButtonDisabled = false;
    
    setState(() {_translatedWord = translation['translation'];});
  }

  Future<void> _addFlashcard() async {
    if (_wordToTranslate == '' || _translatedWord == '') {
      isAddButtonDisabled = true;
      return;
    }
    
    _wordToTranslate = _wordToTranslate.toLowerCase()[0].toUpperCase() +
        _wordToTranslate.toLowerCase().substring(1);
    _translatedWord = _translatedWord.toLowerCase()[0].toUpperCase() +
        _translatedWord.toLowerCase().substring(1);
    
    if (await widget.flashcardsCollection
        .checkIfFlashcardExists(_wordToTranslate, _translatedWord)) {
    } else {
      widget.addRow({
        'front': _wordToTranslate,
        'back': _translatedWord,
        'sourceLang': _sourceLanguage,
        'targetLang': _targetLanguage,
      });
      widget.flashcardsCollection.addFlashcard(
          _wordToTranslate, _translatedWord, _sourceLanguage, _targetLanguage);
      
      widget.updateQuestionText();
    }
    isAddButtonDisabled = true;
  }

  void _openPopup() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Êtes-vous sûr ?'),
          actions: [
            TextButton(
              onPressed: () {
                _addFlashcard();
                Navigator.of(context).pop();
              },
              child: const Text('Oui'),
            ),
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: const Text('Non'),
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text('Source Language', style: TextStyle(fontSize: 18.0),),
                  ElevatedButton(
                    onPressed: () {},
                    child: const Icon(Icons.swap_horiz),
                  ),
                  const Text('Target Language', style: TextStyle(fontSize: 18.0),),
                ],
              ),
              const SizedBox(height: 16.0),
              const TextField(
                textAlign: TextAlign.center,
                decoration: InputDecoration(
                  hintText: 'Word to Translate',
                  border: OutlineInputBorder(),
                ),
              ),
              const SizedBox(height: 16.0),
              const Text('Translation', style: TextStyle(fontSize: 18.0),),
              Expanded(
                child: Container(),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(child:
                  ElevatedButton(
                    onPressed: () {},
                    child: const Text('Translate'),
                  )
                  ),
                  const SizedBox(width: 16.0),
                  Expanded(child: 
                  ElevatedButton(
                    onPressed: () {},
                    child: const Text('Add'),
                  )),
                ],
              ),
            ],
          ),
        ),
    );
  }
}
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
//     Container(padding: const EdgeInsets.all(8.0), child: 
//       Column(children: [
//         Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          
//           Text(
//             _sourceLanguage
//           ),
          
//           ElevatedButton(
//             onPressed: () {setState(() {_swapContent();});}, 
//             child: const Icon(Icons.compare_arrows),
//           ),
          
//           Text(
//             _targetLanguage
//           ),],

//         ),

//         Center(child: 
//           TextField(decoration: 
//             const InputDecoration(
//               border: OutlineInputBorder(),
//               labelText: 'Enter Text',
//             ),
//             onChanged: (value) {setState(() {_wordToTranslate = value;});},
//           )
//         ),

//         Text(style: Theme.of(context).textTheme.headlineSmall,
//           _translatedWord
//         ),

//         Expanded(child: 
//           Align(alignment: Alignment.bottomCenter, child:
//               Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [      

//                 ElevatedButton(
//                   onPressed: isTranslateButtonDisabled ? null : () async {_translate();},
//                   child: const Row(
//                     children: [
//                       Icon(Icons.translate),
//                       Text(
//                         "Traduire"
//                         )
//                     ]
//                   )
//                 ),

//                 ElevatedButton(
//                   onPressed: isAddButtonDisabled ? null : _openPopup,
//                   child: const Row(
//                     children: [
//                       Icon(Icons.add),
//                       Text('Ajouter')
//                     ],
//                   )
//                 ),            
//               ])
//             )
//           )
//         ],
//       )   
//     );
//   }
// }



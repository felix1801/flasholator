// HomePage widget with 3 tabs : Traduire, Réviser and Paquet
import 'utils/deepl_translator.dart';
import 'package:flutter/material.dart';
import 'utils/flashcards_collection.dart';
import 'translate_tab.dart';
import 'review_tab.dart';
import 'data_table_tab.dart';
// import 'package:permission_handler/permission_handler.dart';

class HomePage extends StatefulWidget {
  final FlashcardsCollection flashcardsCollection;
  final DeeplTranslator deeplTranslator;

  const HomePage(
      {Key? key,
      required this.flashcardsCollection,
      required this.deeplTranslator})
      : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final dataTableTabKey = GlobalKey<DataTableTabState>();
  final reviewTabKey = GlobalKey<ReviewTabState>();

  // @override
  // void initState() async {
  //   super.initState();
  //   // The initState() method is called when the stateful widget is inserted into the widget tree
  //   PermissionStatus result = await Permission.storage.request();
  // }

  void dataTableTabFunction(Map<dynamic, dynamic> row) {
    dataTableTabKey.currentState?.addRow(row);
  }

  void reviewTabFunction() {
    reviewTabKey.currentState?.updateQuestionText();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          bottom: const TabBar(
            tabs: [
              Tab(icon: Icon(Icons.translate)),
              Tab(icon: Icon(Icons.replay)),
              Tab(icon: Icon(Icons.folder)),
            ],
          ),
          title: const Text('Flasholator'),
        ),
        body: TabBarView(
          children: [
            TranslateTab(
                flashcardsCollection: widget.flashcardsCollection,
                deeplTranslator: widget.deeplTranslator,
                addRow: dataTableTabFunction,
                updateQuestionText: reviewTabFunction),
            ReviewTab(
                flashcardsCollection: widget.flashcardsCollection,
                key: reviewTabKey),
            DataTableTab(
              flashcardsCollection: widget.flashcardsCollection,
              key: dataTableTabKey,
              updateQuestionText: reviewTabFunction,
            )
          ],
        ),
      ),
    );
  }
}

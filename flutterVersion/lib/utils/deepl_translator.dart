import 'dart:convert';
import 'package:http/http.dart' as http;

class DeeplTranslator {
  final String authKey = "92549361-8983-0e46-4f5f-185b786cedb0:fx";
  // final String url = 'https://api-free.deepl.com/v2/translate';

  Future<Map<String, dynamic>> translate(
      String textToTranslate, String targetLang, String sourceLang) async {
    try {
      final response = await http.post(
        Uri.parse('https://api-free.deepl.com/v2/translate'),
        body: {
          'auth_key': authKey,
          'text': textToTranslate,
          'target_lang': targetLang,
          'source_lang': sourceLang,
          'formality': 'less',
        },
      );

      if (response.statusCode == 200) {
        final json = jsonDecode(utf8.decode(response.bodyBytes));
        final sourceLang = json['translations'][0]['detected_source_language'];
        final translation = json['translations'][0]['text'];
        return {'source_lang': sourceLang, 'translation': translation};
      } else {
        print('Request failed with status: ${response.statusCode}');
        return {'sourceLang': null, 'translation': null};
      }
    } catch (e, stackTrace) {
      print('Error: $e');
      print('Stack Trace: $stackTrace');
      return {'sourceLang': null, 'translation': 'Erreur de connexion'};
    }
  }
}

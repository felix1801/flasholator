// DataTableTab widget
import 'package:flutter/material.dart';
import 'utils/flashcards_collection.dart';

class DataTableTab extends StatefulWidget {
  final FlashcardsCollection flashcardsCollection;
  final Function() updateQuestionText;

  const DataTableTab({
    Key? key,
    required this.flashcardsCollection,
    required this.updateQuestionText,
  }) : super(key: key);

  @override
  State<DataTableTab> createState() => DataTableTabState();
}

class DataTableTabState extends State<DataTableTab> {
  List<Map<dynamic, dynamic>> data = [];

  @override
  void initState() {
    super.initState();
    _fetchData();
  }

  Future<void> _fetchData() async {
    List<Map<dynamic, dynamic>> fetchedData =
        await widget.flashcardsCollection.loadData();
    setState(() {
      data = fetchedData.where((row) => (row['id'] % 2) == 1).toList();
    });
  }

  void addRow(Map<dynamic, dynamic> row) {
    setState(() {
      data.add(row);
    });
  }

  void removeRow(Map<dynamic, dynamic> row) {
    widget.flashcardsCollection.removeFlashcard(row['front'], row['back']);
    setState(() {
      data.removeAt(data.indexOf(row));
    });
  }

  void editRow(String? newText, Map<dynamic, dynamic> row, String key) {
    final front = row['front'];
    final back = row['back'];
    final newFront = key == 'front' ? newText : row['front'];
    final newBack = key == 'back' ? newText : row['back'];

    if (data.indexOf(row) != -1 && newText != null) {
      widget.flashcardsCollection.editFlashcard(front, back, newFront, newBack);
      setState(() {
        row[key] = newText;
      });
      widget.updateQuestionText();
    }
  }

  void _openEditPopup(Map<dynamic, dynamic> row, String key) {
    String? newText;

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Edit Word'),
          content: TextField(
            controller: TextEditingController(text: row[key]),
            onChanged: (String value) {
              newText = value;
            },
          ),
          actions: [
            TextButton(
              onPressed: () {
                editRow(newText, row, key);
                Navigator.of(context).pop();
              },
              child: const Text('Modifier'),
            ),
            TextButton(
              onPressed: () {
                _openConfirmPopup(row, context);
              },
              child: const Text('Supprimer'),
            ),
          ],
        );
      },
    );
  }

  void _openConfirmPopup(
      Map<dynamic, dynamic> row, BuildContext parentContext) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Êtes-vous sûr ?'),
          actions: [
            TextButton(
              onPressed: () {
                removeRow(row);
                Navigator.of(parentContext).pop();
                Navigator.of(context).pop();
              },
              child: const Text('Oui'),
            ),
            TextButton(
              onPressed: Navigator.of(context).pop,
              child: const Text('Non'),
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final tabSize = MediaQuery.of(context).size.height - 144.0;
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
      double scaffoldHeight = constraints.maxHeight;
      print(scaffoldHeight);
      return Scaffold(
        body: Padding(
          padding: EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Expanded(
                child: PaginatedDataTable(
                  headingRowHeight: kMinInteractiveDimension,
                  columns: [
                    DataColumn(
                      label: SizedBox(child: Text('Col SL'), width: constraints.maxWidth*0.15,),
                    ),
                    DataColumn(
                      label: SizedBox(child: Text('front'), width: constraints.maxWidth*0.35,),
                    ),
                    DataColumn(
                      label: SizedBox(child: Text('back'), width: constraints.maxWidth*0.35,),
                    ),
                    DataColumn(
                      label: SizedBox(child: Text('Col TL'), width: constraints.maxWidth*0.15,),
                    ),
                  ],
                  rowsPerPage: (scaffoldHeight.floor() / kMinInteractiveDimension.ceil()).floor() - 3 ,
                  source: _DataSource(data),
                  
                ),
                
              ),
            
            ],
          ),
        ),
      );
    });
  }
}

class _DataSource extends DataTableSource {
  final List<Map> data;

  _DataSource(this.data);

  @override
  DataRow? getRow(int index) {
    final rowData = data[index];
    return DataRow(cells: [
      DataCell(Text(rowData['sourceLang'])),
      DataCell(Text(rowData['front'])),
      DataCell(Text(rowData['back'])),
      DataCell(Text(rowData['targetLang'])),
    ]);
  }

  @override
  int get rowCount => data.length;

  @override
  bool get isRowCountApproximate => false;

  @override
  int get selectedRowCount => 0;
}
    
    
    
    
    
    


















    
    
    
    
    
    
    
    
    
//     SingleChildScrollView(
//       scrollDirection: Axis.vertical,
//       child: DataTable(
//         columns: const <DataColumn>[
//           DataColumn(label: Text('Source lang')),
//           DataColumn(label: Text('Front')),
//           DataColumn(label: Text('Back')),
//           DataColumn(label: Text('Target lang')),
//         ],
//         rows: data.map((Map<dynamic, dynamic> row) {
//           return DataRow(cells: <DataCell>[
            
//             DataCell(Text(row['sourceLang'] as String)),
            
//             DataCell(GestureDetector(
//                 onTap: () {setState(() {_openEditPopup(row, 'front');});},
//                 child: Text(row['front'] as String))),
            
//             DataCell(GestureDetector(
//                 onTap: () {_openEditPopup(row, 'back');},
//                 child: Text(row['back'] as String))),
            
//             DataCell(Text(row['targetLang'] as String)),
          
//           ],);
//         }).toList(),
//       ),
//     );
//   }
// }

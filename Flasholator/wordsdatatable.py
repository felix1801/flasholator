#!/usr/bin/env python3
# -*- coding: utf-8 -*-

FRONT_INDEX = 1
BACK_INDEX = 2

import os

from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.datatables import MDDataTable
from kivymd.uix.dialog import MDDialog

from kivy.lang import Builder


Builder.load_file(os.path.join("styles", "wordsdatatable.kv"))


class EditFlashcardPopupContent(MDBoxLayout):
    pass


class RemoveFlashcardConfirmPopupContent(MDBoxLayout):
    pass


class WordsDataTable(MDDataTable):
    def __init__(self, **kwargs):
        """
        Init the 'WordsDataTable' class
        """
        super().__init__(**kwargs)
        self.bind(on_row_press=self._open_edit_word_popup)


    def _get_clicked_row_index(self, instance_cell):
        """
        Return the index of the row clicked by the user
        """
        page_first_row_index = int(self.pagination.ids.label_rows_per_page.text.split("-")[0]) 
        page_row_index = int(instance_cell.index / len(self.column_data))
        row_index = page_first_row_index + page_row_index - 1

        return row_index


    def _open_edit_word_popup(self, instance_table, instance_cell):
        """
        Open the 'edit_flashcard_popup' and init it
        """
        cell_index = instance_cell.index % 4
        if cell_index==FRONT_INDEX or cell_index==BACK_INDEX:
            # Init the 'edit_flashcard_popup'
            edit_flashcard_popup = MDDialog(elevation=0, type="custom", content_cls=EditFlashcardPopupContent())
            edit_flashcard_popup.content_cls.edit_flashcard_popup = edit_flashcard_popup
            edit_flashcard_popup.content_cls.words_data_table = self
            edit_flashcard_popup.content_cls.instance_cell = instance_cell
            edit_flashcard_popup.content_cls.ids.editable_clicked_word.text = instance_cell.text
            edit_flashcard_popup.open()


    def _open_remove_flashcard_confirm_popup(self, edit_flashcard_popup, instance_cell):
        """
        Open the 'remove_flashcard_confirm_popup' and init it
        """
        remove_flashcard_confirm_popup = MDDialog(elevation=0, type="custom", content_cls=RemoveFlashcardConfirmPopupContent())
        remove_flashcard_confirm_popup.content_cls.remove_flashcard_confirm_popup = remove_flashcard_confirm_popup
        remove_flashcard_confirm_popup.content_cls.edit_flashcard_popup = edit_flashcard_popup
        remove_flashcard_confirm_popup.content_cls.instance_cell = instance_cell
        remove_flashcard_confirm_popup.open()


    def get_then_change_word_values(self, editable_clicked_word, instance_cell):
        """
        Change the value of the word in the data table and the database
        """
        new_word = editable_clicked_word.text

        row_index = self._get_clicked_row_index(instance_cell)
        row = self.row_data[row_index]
        cell_index = instance_cell.index % 4
        new_row = list(row)
        new_row[cell_index] = new_word

        front = row[FRONT_INDEX]
        back = row[BACK_INDEX]
        new_front = new_row[FRONT_INDEX]
        new_back = new_row[BACK_INDEX]

        self.update_row(
            self.row_data[row_index],
            new_row
        )

        return front, back, new_front, new_back


    def get_then_remove_row(self, instance_cell):
        """
        Remove the row from the data table and the database
        """
        row_index = self._get_clicked_row_index(instance_cell)
        row = self.row_data[row_index]

        front = row[FRONT_INDEX]
        back = row[BACK_INDEX]

        self.remove_row(self.row_data[row_index])
        
        return front, back

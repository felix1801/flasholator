#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from datetime import date, timedelta

from sqlalchemy import Column, Integer, String, Date
from sqlalchemy.ext.declarative import declarative_base

from supermemo2 import SMTwo #pip3 install -U supermemo2

Base = declarative_base()

class Flashcard(Base):
    """
    A flashcard, with front and back sides, and properties for SuperMemo2 algorithm.
    """
    __tablename__ = 'flashcards'

    id = Column(Integer, primary_key=True)
    front = Column(String)
    back = Column(String)
    source_lang = Column(String)
    target_lang = Column(String)
    quality = Column(Integer)
    easiness = Column(String, default=str(2.5))
    interval = Column(Integer, default=1)
    repetitions = Column(Integer, default=0)
    times_reviewed = Column(Integer)
    last_review_date = Column(Date)
    next_review_date = Column(Date)
    added_date = Column(Date)


    def __init__(self, front, back, source_lang, target_lang):
        """
        Initialize the flashcard with default SuperMemo2 properties.
        """
        self.front = front
        self.back = back
        self.source_lang = source_lang
        self.target_lang = target_lang
        self.quality = None
        self.easiness = str(2.5)
        self.interval = 1
        self.repetitions = 0
        self.times_reviewed = 0
        self.last_review_date = None
        self.next_review_date = None
        self.added_date = date.today()

    def review(self, quality):
        """
        Review the flashcard, and update its SuperMemo2 properties based on the user's response.
        """
        if quality not in [2, 3, 4, 5]:
            raise ValueError("Quality must be an integer between 2 and 5")

        self.quality = quality

        if self.repetitions == 0:
            # First review.
            review_data = SMTwo.first_review(quality)
        else:
            # Subsequent review.
            review_data = SMTwo(float(self.easiness), self.interval, self.repetitions).review(quality)

        self.easiness = str(review_data.easiness)
        self.interval = review_data.interval
        self.repetitions = review_data.repetitions
        self.times_reviewed += 1
        self.last_review_date = date.today()
        self.next_review_date = review_data.review_date if (quality != 2) else (review_data.review_date - timedelta(1)) 

    def is_due(self):
        """
        Check if the flashcard is due for review.
        """
        return (self.last_review_date is None) or (date.today() >= self.next_review_date)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os

from kivy.lang import Builder

from kivymd.uix.boxlayout import MDBoxLayout

Builder.load_file(os.path.join("styles", "welcomepopupcontent.kv"))

class WelcomePopupContent(MDBoxLayout):
    pass

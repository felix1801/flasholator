#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import traceback
import pdb

import kivy
kivy.require("2.1.0")

import kivymd
from kivymd.app import MDApp

from kivy.core.window import Window
from kivy.lang import Builder
from kivy.utils import platform

from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.anchorlayout import MDAnchorLayout
from kivymd.uix.tab import MDTabsBase
from kivymd.uix.dialog import MDDialog

from welcomepopupcontent import WelcomePopupContent
from wordsdatatable import WordsDataTable

from kivmob import KivMob, TestIds

from deepltranslator import DeeplTranslator
from flashcardcollection import FlashcardCollection


class TranslateTab(MDBoxLayout, MDTabsBase):
    pass


class ReviewTab(MDBoxLayout, MDTabsBase):
    pass


class OptionsTab(MDBoxLayout, MDTabsBase):
    pass


class WordsTableTab(MDAnchorLayout, MDTabsBase):
    pass


class AddCardConfirmPopupContent(MDBoxLayout):
    pass


class OptionButtonsLayout(MDBoxLayout):
    pass


class ReplaceDatabaseConfirmPopup(MDDialog):
    def __init__(self, file_path):
        super().__init__()
        self.file_path = file_path


class FlasholatorApp(MDApp):
    def build(self):
        """
        Build the main application window.

        Returns:
            The root widget of the application.

        """
        # Initialize ads
        # self.init_ads()

        # Set keyboard to text input mode
        Window.keyboard_anim_args = {"d": .2, "t": "in_out_quart"}

        # Set keyboard to be under the clicked widget
        Window.softinput_mode = "below_target"

        # Load the application's KV file
        return Builder.load_file(os.path.join("styles", "main.kv"))



    def on_start(self):
        """
        Event handler for the application start.

        This function is called when the application starts.

        It performs various initialization tasks such as requesting Android permissions,
        initializing popups, tabs, working objects, and setting up the words data table.

        """
        self.ask_android_permissions()
        self.text_to_translate = None
        self.init_popups()
        self.init_tabs()
        self.init_working_objects()
        self.update_question_label()
        self.show_welcome_popup()
        self.set_words_data_table()


    def init_ads(self):
        """
        This function initializes the advertisements using the KivMob library.
        It sets up the ads object, adds a test device, creates a new interstitial ad,
        and requests the interstitial ad.

        """
        self.ads = KivMob(TestIds.APP)
        self.ads.add_test_device("90ADDAE3FD643E36EF853889F92605FE")
        self.ads.new_interstitial(TestIds.INTERSTITIAL)
        self.ads.request_interstitial()


    def ask_android_permissions(self):
        """
        This function asks for storage access permissions if the device is running on Android.
        It checks the platform and requests the necessary permissions using the android.permissions module.
        It also sets the default storage path depending on the operating system.

        """
        # Check if the device is running on Android
        if platform == "android":

            # Import necessary modules for Android permissions
            from android.storage import primary_external_storage_path
            from android.permissions import request_permissions, Permission

            # Request storage access permissions
            request_permissions([
                Permission.WRITE_EXTERNAL_STORAGE,
                Permission.READ_EXTERNAL_STORAGE
            ])

        # Set the default storage path depending on the device
        self.primary_ext_storage = primary_external_storage_path() if platform == "android" else os.environ["HOME"]


    def init_popups(self):
        """
        This function initializes the different popups used in the application.

        """
        # Initialize the 'add_flashcard_confirm_popup'
        self.add_flashcard_confirm_popup = MDDialog(
            elevation=0,
            type="custom",
            content_cls=AddCardConfirmPopupContent()
        )


    def init_tabs(self):
        """
        This function creates and configures the different tabs used in the application.
        It creates instances of TranslateTab, ReviewTab, WordsTableTab.
        Then, it adds TranslateTab and ReviewTab to the tab widget.

        """
        # Create instances of each tab
        self.translate_tab = TranslateTab()
        self.review_tab = ReviewTab()
        self.words_table_tab = WordsTableTab()

        # Add the instances tabs to the tab widget
        self.root.ids.tabs.add_widget(self.translate_tab)
        self.root.ids.tabs.add_widget(self.review_tab)
        self.root.ids.tabs.add_widget(self.words_table_tab)


    def init_working_objects(self):
        """
        This function creates and initializes the working objects used in the application.
        It creates instances of DeeplTranslator and FlashcardCollection.

        """
        # Create an instance of each working object
        self.deepl_translator = DeeplTranslator()
        self.flashcard_collection = FlashcardCollection(primary_ext_storage=self.primary_ext_storage)


    def show_welcome_popup(self):
        """
        This function checks if the welcome popup should be shown based on the value
        stored in the "first_time.txt" file. If the value is "True", it shows the popup
        and updates the file value to "False".

        """
        # Read the value of popup_shown from the "first_time.txt" file
        with open("first_time.txt", "r") as f:
            file_contents = f.read().strip()

        # Convert the file contents to a boolean
        file_bool = file_contents.lower() == "true"

        # If the popup has not been shown before, show the popup
        if file_bool:
            welcome_popup = MDDialog(content_cls=WelcomePopupContent())
            welcome_popup.open()

            # Update the file contents to "False"
            with open("first_time.txt", "w") as f:
                f.write("False")


    def switch_lang(self):
        """
        This function retrieves the source and target language values from the TranslateTab widget,
        swaps their values, and updates the text of their respective widgets to reflect the change.

        """
        # Retrieve the source and target language values from the TranslateTab widget
        source_lang = self.translate_tab.ids.source_lang.text
        target_lang = self.translate_tab.ids.target_lang.text

        # Swap the source and target language values by setting the text of their respective widgets to each other
        self.translate_tab.ids.source_lang.text = target_lang
        self.translate_tab.ids.target_lang.text = source_lang


    def translate(self, text_to_translate, target_lang, source_lang):
        """
        Translate the given text to the specified target language.

        This function translates the given text using the DeeplTranslator object and updates the UI elements
        to display the translation and enable the add flashcard button if a valid translation is obtained.
        The source language and translation are stored for future reference.

        Args:
            text_to_translate (str): The text to be translated.
            target_lang (str): The target language to translate the text into.
            source_lang (str): The source language of the text.

        """
        # Check if the text to translate is different than the last one translated
        if text_to_translate != self.text_to_translate and text_to_translate != "":
            # If it's different, toggle the translate_button while the translation is running to avoid multiple attempts when traduction takes time (network)
            # then translate the text using the DeeplTranslator object and store the source language and translation
            self.translate_tab.ids.translate_button.disabled = True
            source_lang, translation = self.deepl_translator.translate(text_to_translate, target_lang, source_lang)
            self.translate_tab.ids.translate_button.disabled = False
        else:
            # If it's the same text, set the source language and translation to None
            source_lang, translation = None, None

        # if there is a source language and translation, update the UI elements
        if source_lang and translation:
            # set the text of the translated text label to the translation and enable the add_flashcard button
            self.translate_tab.ids.translated_text.text = translation
            self.translate_tab.ids.add_flashcard_button.disabled = False

            # store the text to translate, target language, translation, and source language
            self.text_to_translate, self.target_lang = text_to_translate.lower().capitalize(), target_lang
            self.translation, self.source_lang = translation.lower().capitalize(), source_lang


    def add_flashcard(self, add_flashcard_confirm_popup):
        """
        Adds a flashcard to the flashcard collection with the translated text if the translation exists, and disables the 
        'Add Flashcard' button.
        
        Args:
            add_flashcard_confirm_popup: The AddCardConfirmPopup instance that called this method.

        """
        if self.translation:
            # Add the flashcard to the flashcard_collection and the words_data_table if it doesn't already exist
            if not self.flashcard_collection.check_if_flashcard_exist(self.text_to_translate):
                self.flashcard_collection.add_flashcard(self.text_to_translate, self.translation, self.source_lang, self.target_lang)
                self.words_data_table.add_row((self.source_lang, self.text_to_translate, self.translation, self.target_lang))
            
            # Disable the 'Add Flashcard' button
            self.translate_tab.ids.add_flashcard_button.disabled = True

            # Call the 'update_question_label' method to update the question label text
            self.update_question_label()
        
        # Dismiss the confirmation popup
        add_flashcard_confirm_popup.dismiss()


    def update_question_label(self):
        """
        This function updates the question label in the review tab for flashcard revision.
        It retrieves the due flashcards from the flashcard collection and sets the question label
        to the front of the current flashcard. It also updates the visibility and state of the
        "Display Answer" button based on the presence of due flashcards.

        """
        # Get the list of flashcards that are due for revision
        due_flashcards = self.flashcard_collection.due_flashcards()

        # If there are due flashcards
        if due_flashcards:
            # Set the current flashcard to the first due flashcard and add its front to the question label
            self.current_flashcard = due_flashcards[0]
            self.review_tab.ids.question_label.text = self.current_flashcard.front

            # Make the 'Display Answer' button visible and enabled
            self.review_tab.ids.display_answer_button.opacity = 1
            self.review_tab.ids.display_answer_button.disabled = False
        else:
            # If there are no due flashcards, set the question label to indicate it
            self.review_tab.ids.question_label.text = "Pas de carte à réviser aujourd'hui"

            # Make the "Display Answer" button invisible and disabled
            self.review_tab.ids.display_answer_button.opacity = 0
            self.review_tab.ids.display_answer_button.disabled = True


    def on_tab_switch(self, instance_tabs, instance_tab, instance_tabs_label, tab_text):
        """  
        This function is triggered when the user switches between tabs in the app.
        It performs specific actions based on the currently selected tab.

        Args:
            instance_tabs: The instance of the tab widget.
            instance_tab: The instance of the selected tab.
            instance_tabs_label: The instance of the tab label.
            tab_text (str): The text of the selected tab.

        """
        if tab_text == "Réviser".upper():
            if self.review_tab.ids.question_label.text == "Pas de carte à réviser aujourd'hui":
                # Update question when the "Révision" tab is loaded
                self.update_question_label()
            # self.ads.show_interstitial()

        elif tab_text == "Traduire".upper() or tab_text == "Options".upper():
            print("Ongler Traduire ou Options ouvert")
            # self.ads.request_interstitial()


    def display_answer(self):
        """
        This function is called when the user clicks the "Display Answer" button in the Review tab.
        It shows the back of the current flashcard as the answer and enables the option buttons for the user to choose their answer quality.
        
        """
        # Display the back of the current flashcard as answer
        self.review_tab.ids.answer_label.text = self.current_flashcard.back
        
        # Hide the "Display answer" button
        self.review_tab.ids.display_answer_button.opacity = 0
        self.review_tab.ids.display_answer_button.disabled = True
        
        # Show the option buttons for the user to choose their answer
        self.review_tab.ids.quality_buttons_layout.opacity = 1
        self.review_tab.ids.quality_buttons_layout.disabled = False


    def on_quality_button_press(self, quality):
        """
        This function is called when the user selects a quality option (e.g., Good, Okay, Poor) in the Review tab.
        It clears the answer label, hides the option buttons layout, shows the "Display Answer" button, and calls the review function of the FlashcardCollection.

        Args:
            quality (str): The quality value selected by the user.

        """    
        # Clear the answer label and hide the option buttons layout
        self.review_tab.ids.answer_label.text = ""
        self.review_tab.ids.quality_buttons_layout.opacity = 0
        self.review_tab.ids.quality_buttons_layout.disabled = True
        
        # Show the "Display Answer" button and enable it
        self.review_tab.ids.display_answer_button.opacity = 1
        self.review_tab.ids.display_answer_button.disabled = False

         # Call the review function of FlashcardCollection with the front of the current flashcard and the quality value
        self.flashcard_collection.review(self.current_flashcard.front, self.current_flashcard.back, quality)

        # Update the question label to display the next flashcard
        self.update_question_label()


    def set_words_data_table(self):
        """
        Set up and display the words data table.

        This function retrieves the full list of words from the FlashcardCollection object, creates a WordsDataTable widget with the data, and adds it to the WordsTableTab layout.

        """
        # Retrieve the full list of words from the FlashcardCollection object
        data = self.flashcard_collection.get_full_words_list()

        # Create a WordsDataTable widget with the data
        self.words_data_table = WordsDataTable(row_data=data)

        # Add the WordsDataTable widget to the WordsTableTab layout
        self.words_table_tab.add_widget(self.words_data_table)


    def change_word_value(self, edit_flashcard_popup, instance_cell, editable_clicked_word):
        """
        This function is called when a word value needs to be changed. It retrieves the current values and the new values from the WordsDataTable widget, edits the corresponding row in the FlashcardCollection object, updates the question label, and dismisses the popup widget.

        Args:
            popup: The widget that called this function.

        """
        # Retrieve the current values and the new values from the WordsDataTable widget
        front, back, new_front, new_back = self.words_data_table.get_then_change_word_values(editable_clicked_word, instance_cell)

        # Edit the corresponding row in the database object
        self.flashcard_collection.edit_flashcard(front, back, new_front, new_back)

        # Update the question label to reflect the changes
        self.update_question_label()

        # Dismiss the popup widget
        edit_flashcard_popup.dismiss()


    def remove_flashcard(self, edit_flashcard_popup, remove_flashcard_confirm_popup, instance_cell):
        """
        This function is called when a row needs to be removed. It retrieves the values of the row to be removed from the WordsDataTable widget, removes the corresponding row from the FlashcardCollection object and updates the question label. It dismisses the remove row confirmation popup and the caller popup.

        Args:
            edit_flashcard_popup: The popup widget that contains the row to be removed.
            remove_flashcard_confirm_popup: The remove row confirmation popup widget.

        """
        # Retrieve the values of the row to be removed from the WordsDataTable widget
        front, back = self.words_data_table.get_then_remove_row(instance_cell)

        # Remove the corresponding row from the FlashcardCollection object
        self.flashcard_collection.remove_flashcard(front, back)

        # Update the question label to reflect any changes made to the rows
        self.update_question_label()

        # Dismiss the remove row confirmation popup
        remove_flashcard_confirm_popup.dismiss()

        # Dismiss the edit cell popup
        edit_flashcard_popup.dismiss()


if __name__  ==  "__main__":
    FlasholatorApp().run()


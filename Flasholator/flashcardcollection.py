#!/usr/bin/env python3
# -*- coding: utf-8 -*-

DB_DIR_NAME = "Flasholator"
DB_NAME = "flashcards.db"

import random
import os

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from flashcard import Flashcard # import Flashcard model from Flashcard.py
from flashcard import Base # import the SQLAlchemy Base object from Flashcard.py

class FlashcardCollection:
    """
    A collection of flashcards, stored in an SQLite database.
    """
    def __init__(self, primary_ext_storage):
        """
        Initialize the collection with an SQLite database.
        """        
        db_dir_path = os.path.join(primary_ext_storage, DB_DIR_NAME)
        if not os.path.exists(db_dir_path):
            os.makedirs(db_dir_path)

        self.db_path = os.path.join(db_dir_path, DB_NAME)
        self.engine = create_engine(f"sqlite:///{self.db_path}")
        Base.metadata.create_all(self.engine) # create tables if they don't exist
        self.Session = sessionmaker(bind=self.engine)


    def _load_flashcards(self):
        """
        Load all flashcards from the database.
        """
        session = self.Session()
        flashcards = session.query(Flashcard).all()
        session.close()
        
        return flashcards


    def check_if_flashcard_exist(self, front):
        """
        Check if a flashcard already exist in the database
        """
        session = self.Session()

        flashcard_exist = session.query(Flashcard).filter(Flashcard.front.ilike(front.lower())).first()
        
        session.close()
        return flashcard_exist


    def add_flashcard(self, front, back, source_lang, target_lang):
        """
        Add a new flashcard to the database
        """
        session = self.Session()

        flashcard = Flashcard(front, back, source_lang, target_lang)
        reversed_flashcard = Flashcard(back, front, target_lang, source_lang)
        session.add(flashcard)
        session.add(reversed_flashcard)
        session.commit()

        session.close()


    def review(self, front, back, quality):
        """
        Mark a flashcard as reviewed
        """
        session = self.Session()
        flashcard = session.query(Flashcard).filter_by(front=front, back=back).one()
        flashcard.review(quality)
        session.commit()
        session.close()


    def due_flashcards(self):
        """
        Return a list of flashcards that are due for review, in random order
        """
        due_flashcards = [flashcard for flashcard in self._load_flashcards() if flashcard.is_due()]
        random.shuffle(due_flashcards)
        
        return due_flashcards


    def get_full_words_list(self):
        """
        Return a list of all words in the database
        """
        session = self.Session()
        
        # Get only 1 of 2 row
        data = session.query(Flashcard.source_lang, Flashcard.front, Flashcard.back, Flashcard.target_lang) \
            .filter((Flashcard.id % 2) == 1) \
            .all()

        session.close()

        return data


    def remove_flashcard(self, front, back):
        """
        Remove a flashcard from the database
        """
        session = self.Session()

        row = session.query(Flashcard).filter_by(front=front, back=back).first()
        reversed_row = session.query(Flashcard).filter_by(front=back, back=front).first()

        session.delete(row)
        session.delete(reversed_row)   

        session.commit()
        session.close()


    def edit_flashcard(self, front, back, new_front, new_back):
        """
        Edit a flashcard in the database
        """
        session = self.Session()
        
        row = session.query(Flashcard).filter_by(front=front, back=back)
        reversed_row = session.query(Flashcard).filter_by(front=back, back=front)
        
        row.update({Flashcard.front: new_front, Flashcard.back: new_back})
        reversed_row.update({Flashcard.front: new_back, Flashcard.back: new_front})
        
        session.commit()
        session.close()


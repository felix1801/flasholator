#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import traceback

#import requests
import deepl

class DeeplTranslator:
    def __init__(self):
        self.auth_key = "92549361-8983-0e46-4f5f-185b786cedb0:fx"
        #self.url = 'https://api-free.deepl.com/v2/translate'
        self.translator = deepl.Translator(self.auth_key, send_platform_info=False)

    def translate(self, text_to_translate, target_lang, source_lang):
        # Requests method
        """
        params = {
            'auth_key': self.auth_key,
            'text': text_to_translate,
            'target_lang': target_lang
        }
        
        try:
            response = requests.post(self.url, data=params)
            response.raise_for_status()  # Raise an exception for 4xx or 5xx response status codes
            source_lang = response.json()['translations'][0]['detected_source_language']
            translation = response.json()['translations'][0]['text']
            return source_lang, translation
        except Exception as e:
            traceback.print_exc()
            print(str(e))
            return None, None
        """

        # DeepL method
        try:
            result = self.translator.translate_text(text_to_translate, target_lang=target_lang, source_lang=source_lang, formality='less')
            translation = result.text
            return source_lang, translation
        except Exception as e:
            traceback.print_exc()
            print(str(e))
            return None, None
        
